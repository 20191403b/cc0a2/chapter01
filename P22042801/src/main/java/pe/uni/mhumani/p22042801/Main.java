/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.uni.mhumani.p22042801;

/**
 *
 * @author Miguel Huamani <miguel.huamani.r@uni.pe>
 */
public class Main {
    public static void main(String [] args){
        System.out.println("Hola");
        //cp.original.txt copia.txt
        System.out.println("Length:"+args.length);
        System.out.println("arga[0]"+args[0]); 
        System.out.println("arga[1]"+args[1]); 
        System.out.println("arga[2]"+args[2]); 
        int a=Integer.valueOf(args[0]);
        int b=Integer.valueOf(args[1]);
        int c=Integer.valueOf(args[2]);//error capa 8 
        System.out.println(a);
        System.out.println(b);
        System.out.println(c);
        System.out.println(args[0]+args[1]);
        System.out.println(a+b);
        System.out.println("suma :"+a+b);//(forma incorrecta)el sistema operativo hace la conversión de entero a string, no se nota
        System.out.println(a+b+" es suma");
        System.out.println("suma :"+String.valueOf(a+b));//esto es la forma correcto
        System.out.println("Aritmetic operations");
        System.out.println(a+b);
        System.out.println(a-b);
        System.out.println(a*b);
        System.out.println(a/b);
        System.out.println(a%b);
        System.out.println("Unary Operators");
        System.out.println(a);
        System.out.println(a++);//primero lee la variables y luego hace la operación, por lo que primero imprime y luego suma
        System.out.println(a);
        System.out.println(a--);
        System.out.println(a);
        System.out.println("Relational operations");
        System.out.println(a==b);
        System.out.println(a!=b);
        System.out.println(a>b);
        System.out.println(a>=b);
        System.out.println(a<b);
        System.out.println(a<=b);
        System.out.println("bitwise operators");//en bits
        System.out.println(4&7);//operación "o" en binario
        /*
        2^7  2^6  2^5  2^4  2^3  2^2  2^1  2^0 
        0    0    0    0    0    1     0    0 
        0    0    0    0    0    1     1    1
        */
        System.out.println("Logical operations");
        boolean b1=true;
        boolean b2=false;
        System.out.println(b1&&b2);
        System.out.println(b1||b2);
        System.out.println(b1&&!b2);
        System.out.println(!b1||b2);
        
        System.out.println("assignment operators");
        int d=4;
        System.out.println(d);
        d+=8;
        System.out.println(d);
        d-=2;
        System.out.println(d);
        
        System.out.println("conditional operators");
        int result =(a==b)?1:2;
        System.out.println("result: "+result);
        a=5;
        b=4;
        result =(--a==b)?1:2;
        System.out.println(result);
        
    }
}
