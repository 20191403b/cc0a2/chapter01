/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.uni.mhumani.p22050702;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
/**
 *
 * @author Miguel Huamani <miguel.huamani.r@uni.pe>
 */
public class Main {
  public static void main (String[] args) throws FileNotFoundException, IOException{
      System.out.println("Files");
      
      String name="ByteStream.txt";
      File file=new File(name);
      
      System.out.println("Path:  "+file.getAbsolutePath());
      System.out.println("Writting a file");
      
      FileOutputStream fileOutputStream= new FileOutputStream(file);
      
      fileOutputStream.write(77);
      fileOutputStream.write(73);
      fileOutputStream.write(71);
      fileOutputStream.write(85);
      fileOutputStream.write(69);
      fileOutputStream.write(76);
      fileOutputStream.close();
      
      System.out.println("Read a file");
      FileInputStream fileInputStream = new FileInputStream(file);
      int decimal;
      while((decimal=fileInputStream.read())!=-1){
          System.out.print((char)decimal);
      }
      System.out.println("");
      
      
      //la siguiente forma es más compleja
      name="CharacterStream.txt";
      file=new File(name);
      System.out.println("Paht : "+file.getAbsolutePath());
      System.out.println("Write a file");
      FileWriter fileWriter=new FileWriter(file);
      fileWriter.write(77);
      fileWriter.write(73);
      fileWriter.write(71);
      fileWriter.write(85);
      fileWriter.write(69);
      fileWriter.write(76);
      fileWriter.close();
      System.out.println("Read file");
      FileReader fileReader=new FileReader(file);
      while((decimal=fileReader.read())!=-1){
         System.out.print((char)decimal);
      }
      System.out.println("");
      
      System.out.println("List of file");
      String paths[];
      file=new File("./");
      System.out.println("Directory: "+file.getAbsolutePath());
      paths=file.list();
      for(String path:paths){
          System.out.println(path);
      }
      
      System.out.println("Create Directories");
      String directory="/Files/Binaries/Selected";
      String fullPath=file.getAbsolutePath()+directory;
      file=new File(fullPath);
      if(file.mkdirs()){
          System.out.println("Directries has been created");
      }else{
           System.out.println("Directries has already been created ");
      }
  }  
}
