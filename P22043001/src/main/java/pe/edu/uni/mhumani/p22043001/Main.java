/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.uni.mhumani.p22043001;

/**
 *
 * @author Miguel Huamani <miguel.huamani.r@uni.pe>
 */
public class Main {
   public static void main(String[]args){
        System.out.println("Loop statements");
        System.out.println("For Loop");
        //int i;//el contexto de i está en la función main
              //
        for(int i=0;i<10;i++){
            System.out.println(i);
        }
        //System.out.println(++i);
        {
          int j=0;
          System.out.println(++j);
        }
        //System.out.println(++j);
        for(int i=9;0<=i;i--){
            System.out.println(i);
        }
        
        //matriz 5x3
        int f=5;
        int c=3;
        for(int i=1;i<=f;i++){
          // System.out.println("i:"+i); 
          for(int j=1;j<=c;j++){
              System.out.print("( "+i+" , "+j+" )");
          }  
          System.out.println("");
        }
        
        System.out.println("While loop");
        int i=0;
        while(i<10){
            i++;
            System.out.println(i);
        }
        i=10;
        while(0<i){
            System.out.println(i);
            i--;
        }
        
        i=1;
        while(i<=f){
            int j=1;
            while(j<=c){
                System.out.print("( "+i+" , "+j+" )");
                j++;
            }
            System.out.println("");
            i++;
        }
        
        System.out.println("---");
        i=0;
        while(i<0){
           System.out.println("this mesage is not printed");
        }    
        
        do{
           System.out.println("this mesage is printed");
        }while(i<0);
        
        i=1;
        do{
          int j=1;  
          do{
              System.out.print("( "+i+" , "+j+" )");
              j++;
          }while(j<=c);
          System.out.println("");  
          i++; 
        }while(i<=f);
        
        //print primer numbers betweens 10 and 90
        
        for(int p=10;p<=90;p++){
            boolean t=true;
        for(int j=2;j<p;j++){
            if(p%j==0){
                //System.out.println(p+" is not prime");
                t=false; 
                break;
            }
        }
        if(p==53)continue;//continues es una ecepción, y breack detiene el bucle
        if(t){
             System.out.println(" is prime "+p);
        }
        
        }
        
   }   
}
