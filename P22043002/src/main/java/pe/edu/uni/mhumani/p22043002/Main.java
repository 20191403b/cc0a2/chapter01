/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.uni.mhumani.tictactoe;

import static java.lang.Math.E;
import static java.lang.Math.PI;
import static java.lang.Math.cos;
import static java.lang.Math.log;
import static java.lang.Math.pow;
import static java.lang.Math.round;
import static java.lang.Math.sin;
import static java.lang.Math.sqrt;

/**
 *
 * @author Miguel Huamani <miguel.huamani.r@uni.pe>
 */
public class Main {
    public static void main(String[]args){
         System.out.println("Math Functions");
         float i=10.25f;
         //round :redondeo
         System.out.println("i: "+round(i));
         
         //log
          System.out.println(E);
          System.out.println("ln(e) "+log(E));
          //power
          System.out.println("2^8: "+pow(2,8));
          System.out.println("256^(1/2): "+pow(256,0.5));
          //sqrt
          System.out.println("256^(1/2): "+sqrt(259));
          //trigonometric functions
          System.out.println("sen(0)): "+sin(0));
          System.out.println("sen(pi/2)): "+sin(PI/2));
          System.out.println("xoa(pi)): "+cos(PI));
          
    }
}
