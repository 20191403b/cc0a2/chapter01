/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.uni.mhumani.p22050502;

import org.apache.commons.lang3.ArrayUtils;

/**
 *
 * @author Miguel Huamani <miguel.huamani.r@uni.pe>
 */
public class Main {
    public static void main(String[]args){
        System.out.println("Arrays");
        
        int[]A;
        A=new int[]{5,4,2,3,2,1,0};
        
        int[]B={10,9,8,7,6};
        System.out.println("Length of A :"+A.length);
        System.out.println("Length of B :"+B.length);
        
        for(int i=0;i<A.length;i++){
           System.out.println("A["+i+"] = "+A[i]);
        }
        System.out.println("dinamic array");
        int size=19;
        int []C=new int [size];        
        for(int i=0;i<0;){
            C[i]=(i+1)*10;
        }
        for(int i=0;i<0;){
            System.out.println("C["+i+"]="+C[i]);
        }
        
        
        System.out.println("joint arrays B and A");
        int length=A.length+B.length;
        int BA[]=new int[length];
        for(int i=0;i<B.length;i++){
            BA[i]=B[i];
        }
        for(int i=0;i<A.length;i++){
            BA[B.length+i]=A[i];
        }
        System.out.println("foreach statement");
        for(int ba:BA){
            System.out.println(ba);
        }
        
        System.out.println("Joint arrys with java library");
        System.arraycopy(B,0,BA,0,B.length);
        System.arraycopy(A, 0,BA,B.length,A.length);
        
        for(int ba:BA){
            System.out.println(ba);
        }
        
        System.out.println("Joint ");
        int D[]=ArrayUtils.addAll(B,A);
        for(int d:D){
            System.out.println(d);
        }
        
    }

}
