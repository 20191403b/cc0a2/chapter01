/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.uni.mhumani.p22050701;

/**
 *
 * @author Miguel Huamani <miguel.huamani.r@uni.pe>
 */
public class Main {
   public static void main(String[]args){
       System.out.println("Funcitions");
       Function();
       Function("Miguel");
       Function("Miguel",21);
       String[]res={"Hello","Miguel"};
       Function(res);
       int [] numbers ={5,2,9};
       int sum=Function(numbers);
       System.out.println("Sum: "+sum);
       int a=2;
       int b=3;
       sum=Function(a,b);
       System.out.println("a: "+a+" + b: "+b+" = "+sum);
       
       //valor por referencia, para poder editar los parámetros
       StringBuilder stringNumber=new StringBuilder(); 
       stringNumber.append("0");
       System.out.println("String number"+stringNumber);
       Function(stringNumber);
       System.out.println("String number"+stringNumber);
       stringNumber.setLength(0);
       if(authentication("1",stringNumber)){
           System.out.println("Is OK");
       }else{
            System.out.println("Filled, error "+stringNumber);
       }
   } 
   
   // name functions, arguments type
   public static void Function(){//firma de la función: nos dice el nombre de la fución, los tipos de agumentos ,pero no el tipo de retorno
       System.out.println("Hello world");
   }
   public static void Function(String name){
       System.out.println("Hello "+name);
   }
   public static void Function(String name,int age){
       System.out.println("Hello "+name+" you are "+ String.valueOf(age)+" years old");
   }
   public static void Function(String []args){
       for(String arg:args){
           System.out.print(arg);
       }
       System.out.println("");
   }
   
   public static int Function(int ... numbers){
       int sum=0;
       for(int number :numbers){
           sum+=number;
       }
       return sum;
   }
   public static void Function(StringBuilder reference){
       reference.setLength(0);
       reference.append("1");
       
   }
   /*
   credential    assett
   error         Type error
   return tur if authentication is ok, otherwise false
   */
   public static boolean authentication(String credential,StringBuilder error){
       boolean out=false;
       error.setLength(0);
       if(credential.equals("1")){
           out=true;
           error.append("0");//autentication is ok
       }else{
           error.append("-1");//otherwise
       }
       return out;
   }
   
}
