/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.uni.mhumani.p22051403;

/**
 *
 * @author Miguel Huamani <miguel.huamani.r@uni.pe>
 */
public class Main {
    public static void main(String [] args){
        System.out.println("Objects...");
        Enemy e1=new Enemy();
        Enemy e2=new Enemy();
        System.out.println("ei: "+e1);
        System.out.println("e2: "+e2);
        
        e1.addLife(5);
        e2.addHealt(-20);
        System.out.println("ei: "+e1);
        System.out.println("e2: "+e2);
        //reload game
        
        Enemy e3=new Enemy(15,100);
        e3.bonusLife();
        System.out.println("e3: "+e3);
        
        SuperEnemy e4=new SuperEnemy();
        System.out.println("e4: "+e4);
        e4.bonusLife();
        System.out.println("e4: "+e4);
        e4.bonusShield();
        System.out.println("e4: "+e4);
        
    }
}
