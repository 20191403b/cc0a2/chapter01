/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.uni.mhumani.p22051403;

/**
 *
 * @author Miguel Huamani <miguel.huamani.r@uni.pe>
 */
public class SuperEnemy extends Enemy implements Shield{
    private String name;
    private int shield;
    
    public String getName(){
        return name;
    }
    public void setName (String name){
        this.name=name;
    }
    public int getShield(int shield){
        return shield;
    }
    public void setShield(int shield){
        this.shield=shield;
    }
    
    public SuperEnemy(){
        this.name="Predatos";
        this.shield=1;
    }
    public SuperEnemy(String name){
        super(100,1000);
        this.name=name;
        this.shield=1;
    }
    
    @Override
    public void bonusLife(){
       super.addLife(50);
    }
    @Override
    public String toString(){
        return "Enemy["+" name:"+getName()+" life: "+super.getLife()+" health´:"+super.getHealth()+" shield:"+shield+"}";
    }
    @Override
    public void bonusShield(){
        shield+=5;
    }
}
