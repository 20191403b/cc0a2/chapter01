/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.uni.mhumani.p22051403;

/**
 *
 * @author Miguel Huamani <miguel.huamani.r@uni.pe>
 */
public class Enemy extends Rol {
    int life;
    float health;
    
    public int getLife(){
        return life;
    }
    public void addLife(int life){
        this.life+=life;
    }
    public float getHealth(){
        return health;
    }
    public void addHealt(float health){
        this.health+=health;
    }
    
    public Enemy(){
        life=10;
        health=100;
    }
    public Enemy(int life,float health){
        this.health=health;
        this.life=life;
    }
    @Override
    public String toString(){
        return "Enemy["+" life: "+life+" health´:"+health+"}";
    }
    @Override
    public void bonusLife(){
        life+=5;
    }
}
