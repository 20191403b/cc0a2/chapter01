/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.uni.mhumani.p22050501;

import java.util.Arrays;

/**
 *
 * @author Miguel Huamani <miguel.huamani.r@uni.pe>
 */
public class Main {
    public static void main(String[]args){
        System.out.println("Strings");
        String string =" Hello world !!! ";
        System.out.println(string);
        System.out.println("Length of string:"+string.length());
        System.out.println("Character's location");
        System.out.println("1st character "+string.charAt(0));
        System.out.println("last character "+string.charAt(string.length()-1));
        System.out.println("Convertions");
        System.out.println(string.toUpperCase());
        System.out.println("Splits");
        String[] parts=string.split(" ");
        System.out.println("Parts size: "+parts.length);
        for(int i=0;i<parts.length;i++){
            System.out.println(parts[i]);
        }
        System.out.println("Parts :"+ Arrays.toString(parts));
        System.out.println("Trim");//quita espacio iniciales y finales
        String trim=string.trim();
        System.out.println("Lengt of trim "+trim.length());
        System.out.println("Trim : "+trim);
    }
}
