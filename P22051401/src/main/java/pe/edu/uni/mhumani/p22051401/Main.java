/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.uni.mhumani.p22051401;

/**
 *
 * @author Miguel Huamani <miguel.huamani.r@uni.pe>
 */
public class Main {
    public static void main(String[]args){
        System.out.println("Exeptions...");
        int[] array={10,20,30,40,50,60};
        try{
           int i=0;
           System.out.println(array[i]);
           int j=i/1;
           Function();
           
           System.out.println("End Try");
           
        }catch(ArrayIndexOutOfBoundsException e){//esta excepción hereda de Exception, por lo que el herror sería tratado en el primer catch
            System.out.println("Array Index error");//ocurrirá error si se usa esta exceción antes de 
            System.out.println(e);
        }catch(ArithmeticException e){
            System.out.println("/ by zero");//esta exceción no pertenece a la rrama del anterior, por lo que no hay erro y no se cruzan
            System.out.println(e);
        }
        catch(Exception e){//esta excepción va primero porque es padre del las otras escepciónes 
            System.out.println(e);//siempre debe ser el último
        }finally{//siempre se ejecuta al final del try catch 
            
             System.out.println("End Finaly");
        }
        System.out.println("End of application");
    }
    
    public static void Function()throws ArithmeticException{//funcion para manejar excepcion
        int j=1/0;
        //throw new ArithmeticException();//lanza este tipo de excepxción que se desea
    }//el throw(sin la s) hace que el código funcione para detectar excepciones puestas a propósito
}
