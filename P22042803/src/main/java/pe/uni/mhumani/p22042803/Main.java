/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.uni.mhumani.p22042803;

/**
 *
 * @author Miguel Huamani <miguel.huamani.r@uni.pe>
 */
public class Main {
    public static void main(String []args){
        System.out.println("Conditional Statements");
        /*
        int i=35;
        if(30<i){
            System.out.println("the number is greater to 30");
        }
        if(i%2==1){
            System.out.println("the number is odd");
        }
        
        if(i%5==0){
            System.out.println("the number is multiple of 5");
        }
        */
        /*
        int i=35;
        if(30<i){
            System.out.println("the number is greater to 30");
        }else if(i%2==1){
            System.out.println("the number is odd");
        }else if(i%5==0){
            System.out.println("the number is multiple of 5");
        }else{
            System.out.println("the number is other");
        }
        System.out.println("here the program finish");
        */
        /*
        int i=35;
        if(30<i){
            System.out.println("the number is greater to 30");
            return;
        }
        if(i%2==1){
            System.out.println("the number is odd");
            return;
        }
        if(i%5==0){
            System.out.println("the number is multiple of 5");
            return;
        }
        System.out.println("here the program continue");
        */
        int i=11;
        switch(i%2){
            case 0:
                 System.out.println("number is even");
                 break;
            case 1:
                 System.out.println("number is odd");
                 break;    
            default:
                System.out.println("any other number");     
        }
         System.out.println("continue");
  }
}
