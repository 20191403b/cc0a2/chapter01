/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.uni.mhumani.p22051402;

/**
 *
 * @author Miguel Huamani <miguel.huamani.r@uni.pe>
 */
public class MyException extends Exception {
    String message;
    
    public MyException (String message){
        this.message=message;
    }
    
    @Override
    public String toString(){
        return "My Exception {"+message+"}";
    }
}
