/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.uni.mhumani.p22051402;

/**
 *
 * @author Miguel Huamani <miguel.huamani.r@uni.pe>
 */
public class Main {
    public static void main(String[]args){
       System.out.println("Exeptions 2...");
       try{
           myExceptionFunction();
           Function();
       }catch(ArithmeticException e){
           System.out.println("Arithmetic Exception "+e);
       }catch (MyException e){
           System.out.println("My Exception.. "+e);
       }
       catch(Exception e){
           System.out.println("Exception "+e);
       }
       System.out.println("Finish");
    }
    
     public static void Function()throws ArithmeticException{//funcion para manejar excepcion
        int i=1/0;
    }//
    public static void myExceptionFunction() throws MyException{
        throw new MyException("My error !!");
    } 
}
